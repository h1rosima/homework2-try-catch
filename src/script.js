const books = [
    {
        author: 'Люсі Фолі',
        name: 'Список запрошених',
        price: 70,
    },
    {
        author: 'Сюзанна Кларк',
        name: 'Джонатан Стрейндж і м-р Норрелл',
    },
    {
        name: 'Дизайн. Книга для недизайнерів.',
        price: 70,
    },
    {
        author: 'Алан Мур',
        name: 'Неономікон',
        price: 70,
    },
    {
        author: 'Террі Пратчетт',
        name: 'Рухомі картинки',
        price: 40,
    },
    {
        author: 'Анґус Гайленд',
        name: 'Коти в мистецтві',
    },
];

const divList = document.getElementById('root');
const list = document.createElement('ul');
divList.append(list);
books.forEach((book) => {
    const listItem = document.createElement('li');
    listItem.style.fontSize = '35px';
    listItem.style.margin = '10px';

    try {
        if (!book.author || !book.name || !book.price) {
            listItem.style.display = `none`;

            console.log(book);
        } else {
            // listItem.textContent = `${book.author ? book.author : 'Имя автора не указано'} - ${book.name ? book.name : 'Название не указано'}, ${book.price ? book.price : 'Цена не указана'}. `;

            listItem.textContent = `${book.author} - ${book.name}, ${book.price}$.`;
        }
    } catch (error) {
        alert('не работает, переделывай');
    }

    list.append(listItem);
});
